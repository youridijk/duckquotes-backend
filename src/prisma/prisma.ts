import {PrismaClient} from "@prisma/client";

export const prisma = new PrismaClient();

process.on('SIGINT', async () => {
    console.log('Shutting down');
    prisma.$disconnect();
    process.exit();
});

export function isValidId(id: any) {
    return id != null && !isNaN(id) && id >= 0;
}

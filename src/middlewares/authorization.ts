import {StatusCodes} from "http-status-codes";
import jwt from "jsonwebtoken";
import {PrismaClient} from "@prisma/client";
import {Request, Response, NextFunction} from "express";

const prisma = new PrismaClient()

export async function checkLoggedIn(req: Request, res: Response, next: NextFunction){
    // Verkrijg de authorization en check of deze niet null is
    // IS de header null, geef dan een 401 status terug
    const authHeader = req.headers["authorization"];
    if (!authHeader) {
        return res
            .status(StatusCodes.UNAUTHORIZED)
            .send({error: "Authorization header is missing"});
    }

    // Check of de authorization header geldig is, zo niet geef een 401 status terug
    const headerParts = authHeader.split(" ");
    if (headerParts.length < 2) {
        return res
            .status(StatusCodes.UNAUTHORIZED)
            .send({error: "Invalid authorization header"});
    }

    // Verkrijg het JWT en de payload van ervan
    const token = headerParts[1];
    const payload: any = jwt.decode(token);

    // Check of de payload niet null is
    if (!payload || !payload["id"]) {
        return res
            .status(StatusCodes.UNAUTHORIZED)
            .send({error: "Invalid payload!"});
    }

    const user = await prisma.user.findUnique({
        where: {
            id: payload.id
        },
    });

    if (!user) {
        return res
            .status(StatusCodes.UNAUTHORIZED)
            .send({error: "No user found"});
    }


    // Verifieer het token met het secret van de gebruiker
    jwt.verify(token, user.secret, (error) => {
        // Als het token ongeldig is, geef een 401 status terug
        if (error) {
            return res
                .status(StatusCodes.UNAUTHORIZED)
                .send()
        }

        // Voeg de gebruiker toe aan het request en ga door naar volgende request/middleware
        delete user.secret;
        req.user = user;

        next()
    });
}

export function checkAdmin(req: Request, res: Response, next: NextFunction) {
    // Als de rol admin is, kan het request waar deze middleware aangekoppeld is doorgaan
    // Zo niet, geef een 401 status terug
    if (req.user.role === "admin") {
        next();
    } else {
        return res
            .status(StatusCodes.UNAUTHORIZED) // Misschien moet dit een 403 FORBIDDEN zijn
            .send();
    }
}

export function checkCreator(req: Request, res: Response, next: NextFunction){
    // Als de rol admin is, kan het request waar deze middleware aangekoppeld is doorgaan
    // Zo niet, geef een 401 status terug
    if (req.user.role === "creator") {
        next();
    } else {
        return res
            .status(StatusCodes.UNAUTHORIZED) // Misschien moet dit een 403 FORBIDDEN zijn
            .send({error: "Haha, only creator can do this action!"});
    }
}
import {Request, Response, NextFunction} from "express";
import {PrismaClient} from "@prisma/client";
import {StatusCodes} from "http-status-codes";
import {isValidId} from "../helpers/methods";

const prisma = new PrismaClient();

export function checkQuoteId(req: Request, res: Response, next: NextFunction) {
    const {quoteId} = req.params;

    if (!isValidId(quoteId)) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: `${quoteId} is not a valid quote_id`})
    }

    next();
}

export async function checkQuoteExists(req: Request, res: Response, next: NextFunction) {
    const quoteId = Number(req.params.quoteId);

    const quoteCount = await prisma.quote.count({
        where: {
            id: quoteId,
        }
    });

    if(quoteCount === 0){
        return res
            .status(StatusCodes.NOT_FOUND)
            .send({error: `Quote with id ${quoteId} not found`})
    }

    next()
}

export async function addQuoteToRequest(req: Request, res: Response, next: NextFunction) {
    const {quoteId} = req.params;

    const quote = await prisma.quote.findUnique({
        where: {
            id: Number(quoteId),
        }
    })

    if (!quote) {
        return res
            .status(StatusCodes.NOT_FOUND)
            .send({error: `Quote with id ${quoteId} not found`})
    }

    req.quote = quote;
    next();
}

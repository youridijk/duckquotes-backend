function onload(){
    const isLoggedIn = localStorage.getItem("token") != null;
    document.getElementById("status").innerHTML = isLoggedIn ? `Welkom!!` : 'Log <a href="login.html">hier</a> in';
    if(isLoggedIn) {
        loadQuotes();
    }
}

function formatDate(inputDate) {
    const date = new Date(inputDate);
    const hour = String(date.getHours()).padStart(2, "0");
    const minutes = String(date.getMinutes()).padStart(2, "0");

    const day = String(date.getDate()).padStart(2, "0");
    const month = date.toLocaleString('default', { month: 'long' });
    const year = date.getFullYear();

    return `${hour}:${minutes}, ${day} ${month} ${year}`;
}

function makePElement(text){
    const el = document.createElement("p");
    el.innerText = text;
    return el;
}

function makeQuoteContainer(quote, sayer, creator, date){
    const container = document.createElement("div");
    container.className = "quote-container";

    const quoteElement = document.createElement("h3");
    quoteElement.innerText = quote;

    const sayerElement = makePElement(`Sayer: ${sayer}`);
    const creatorElement = makePElement(`Creator: ${creator}`);
    const dateElement = makePElement(`Gemaakt op ${formatDate(date)}`);

    const pContainer = document.createElement("div");

    pContainer.appendChild(sayerElement);
    pContainer.appendChild(creatorElement);
    pContainer.appendChild(dateElement);

    container.appendChild(quoteElement);
    container.appendChild(pContainer);

    return container;
}

function loadQuotes(){
    const url = "https://duckquotes.herokuapp.com/api/v1/quotes";
    // const url = "http://localhost:8080/api/v1/quotes";
    fetch(url, {
        headers: {
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        }
    }).then(async response => {
        const body = await response.json();

        if(response.ok){
            const quotesContainer = document.getElementById("quotes-container");

            for(const quote of body){
                const quoteElement = makeQuoteContainer(quote.quote, quote.sayer, quote.creator, quote.publication_date);
                quotesContainer.appendChild(quoteElement);
            }
        }else if(response.status === 401){
            alert("Not logged in!");
        }else{
            alert(`Error getting quotes: ${body.error}`);
        }
    })
        .catch(error => {
            alert(`Error getting quotes: ${error}`);
        })
}
function onload(){
    const isLoggedIn = localStorage.getItem("token") != null;
    document.getElementById("status").innerHTML = isLoggedIn ? `Welkom!!` : 'Log <a href="login.html">hier</a> in';
    document.getElementById("form").onsubmit = createQuote;

    if(isLoggedIn) {
        getUserNames();
    }
}

function getUserNames(){
    // const url = "http://localhost:8080/api/v1/users/usernames"
    const url = "https://duckquotes.herokuapp.com/api/v1/users/usernames";

    fetch(url, {
        headers: {
            "Authorization": `Bearer ${localStorage.getItem("token")}`
        }
    })
        .then(async result => {
            const responseBody = await result.json();
            if(result.ok){
                const sayerElement = document.getElementById("sayer");
                for(const username of responseBody){
                    sayerElement.appendChild(createOptionElement(username["username"], username["id"]));
                }
            }else{
                alert(`Fetch succes, but error getting usernames: ${responseBody.error}`);
            }
        })
        .catch(error => {
            alert(`Can't fetch usernames: ${error}`);
        })
}

function createOptionElement(text, value){
    const el = document.createElement("option");
    el.innerText = text;
    el.value = value;
    return el;
}

function createQuote(event){
    event.preventDefault();

    const quote = document.getElementById("quote").value;
    let context = document.getElementById("context").value;
    if(context === ""){
        context = null;
    }

    const sayer = document.getElementById("sayer").value;

    // alert(`${sayer}, ${quote}, ${context}`);
    // const url = "http://localhost:8080/api/v1/quotes";
    const url = "https://duckquotes.herokuapp.com/api/v1/quotes";

    const body = {
        quote,
        context,
        tagged_user_id: sayer
    }

    fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify(body)
    })
        .then(async response => {
            if (response.ok) {
                alert("Aanmaken quote is gelukt!")
            }else{
                const responseBody = await response.json();
                alert(`Fetch succes, maar error tijdens aanmaken quote: ${responseBody.error}`)
            }
        })
        .catch(error => alert(`Error tijdens aanmaken quote: ${error}`))
}
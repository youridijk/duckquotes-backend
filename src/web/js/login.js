function onload(){
    const form = document.getElementById("form");
    form.onsubmit = login;
}

function login(e){
    e.preventDefault();

    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    const url = "https://duckquotes.herokuapp.com/api/v1/authenticate";
    // const url = "http://localhost:8080/api/v1/authenticate";
    fetch(url, {
        method: "POST",
        body: JSON.stringify({username, password}),
        headers: {
            "Content-Type": "application/json"
        }
    })
        .then(async response => {
            const body = await response.json()
            if(response.ok) {
                localStorage.setItem("token", body.token);
                alert("Login gelukt!")
                window.location.href = "quotes.html"
            }else if(response.status === 401){
                alert("Inloggegevens onjuist")
            }else{
                alert(`Request succes, maar error tijdens inloggen: ${body.error}`)
            }
        })
        .catch(error => {
            alert(`Error logging in: ${error}`)
        });

}
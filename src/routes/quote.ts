import {Router} from "express";
import {StatusCodes} from "http-status-codes";

import {checkQuoteExists, checkQuoteId} from "../middlewares/quote";
import {checkLoggedIn} from "../middlewares/authorization";

import {isValidId} from "../helpers/methods";
import {prisma} from "../prisma/prisma";

export const router = Router();

router.use(checkLoggedIn);
router.use("/:quoteId/votes", require("./vote").router);

function addVoteScore(quote: any & { votes: { isPositive: boolean }[] }) {
        quote.vote_score = quote.votes.length === 0 ? 0 : quote.votes
            .map((vote: any & { isPositive: boolean }) => Number(vote.isPositive ? 1 : -1))
            .reduce((v1: number, v2: number) => v1 + v2);
        quote.voteScore = quote.vote_score
        quote.created_at = quote.publicationDate
        return quote;
}

router.get("", async (req, res) => {
    const quotes = await prisma.quote.findMany({
        select: {
            id: true,
            quote: true,
            publicationDate: true,
            sayer: {
                select: {
                    username: true,
                }
            },
            votes: true,
        },
        orderBy: {
            id: "desc"
        }
    });

    const quoteWithVS = quotes.map((quote) => {
        const quoteWithVS = addVoteScore(quote);
        delete quoteWithVS.votes;
        return quoteWithVS;
    });

    res
        .status(StatusCodes.OK)
        .send(quoteWithVS)
});

router.get("/test", async (req, res) => {
    const newQuote = await prisma.quote.findUnique({
        where: {
            id: 1,
        },
        include: {
            votes: {
                select: {
                    user: {
                        select: {
                            username: true
                        }
                    },
                    isPositive: true
                }
            },
            creator: {
                select: {
                    username: true,
                }
            },
            sayer: {
                select: {
                    username: true,
                }
            },
        },
    })

    const votes = newQuote.votes;
    //@ts-ignore
    newQuote.voteCount = votes
        .map((vote) => Number(vote.isPositive ? 1 : -1))
        .reduce((v1, v2) => v1 + v2);


    res
        .status(StatusCodes.OK)
        .send(newQuote)
})

router.get("/:quoteId", checkQuoteId, checkQuoteExists, async (req, res) => {
    const quote = await prisma.quote.findUnique({
        where: {
            id: Number(req.params.quoteId),
        },
        select: {
            id: true,
            quote: true,
            context: true,
            publicationDate: true,
            creator: {
                select: {
                    username: true,
                },
            },
            sayer: {
                select: {
                    username: true,
                },
            },
            votes: {
                select: {
                    user: {
                        select: {
                            username: true,
                        },
                    },
                    isPositive: true
                },
            },
        },
    });

    addVoteScore(quote);

    const myVote = quote.votes.find((vote) => vote.user.username === req.user.username);

    if(myVote){
        // @ts-ignore
        quote.myVotePositive = myVote.isPositive;
    }

    res
        .status(StatusCodes.OK)
        .send(quote)
});

router.post("", async (req, res) => {
    const {id: userId} = req.user;

    const {quote, context, sayerId} = req.body;

    if (!quote || !sayerId) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: "Quote and/or sayer id is missing"});
    }

    if (!isValidId(sayerId)) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: "Tagged user id is invalid"});
    }

    const sayerUserCount = await prisma.user.count({
        where: {
            id: sayerId,
        },
    });

    if (sayerUserCount === 0) {
        return res
            .status(StatusCodes.NOT_FOUND)
            .send({error: `Sayer with id ${sayerId} not found`});
    }

    const newQuote = await prisma.quote.create({
        data: {
            quote,
            context,
            sayerId,
            creatorId: userId,
        },
        select: {
            id: true,
            quote: true,
            context: true,
            publicationDate: true,
            creator: {
                select: {
                    username: true,
                }
            },
            sayer: {
                select: {
                    username: true,
                }
            },
        },
    })

    // @ts-ignore
    newQuote.voteScore = 0;

    res
        .status(StatusCodes.CREATED)
        .send(newQuote)
});

router.put("/:quoteId", checkQuoteId, checkQuoteExists, async (req, res) => {
    const quoteId = Number(req.params.quoteId);

    const {quote, context, sayerId} = req.body;

    if (!quote || !sayerId) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: "Quote and/or sayerId is missing"});
    }

    await prisma.quote.update({
        where: {
            id: quoteId,
        },
        data: {
            quote,
            context,
            sayerId,
        }
    })

    res
        .status(StatusCodes.NO_CONTENT)
        .send();

});

router.delete("/:quoteId", checkQuoteId, checkQuoteExists, async (req, res) => {
    const quoteId = Number(req.params.quoteId);
    const quote = req.quote;

    // Dit if statement zorgt ervoor dat een user met creator role ook quotes mag verwijderen waarvan de creator null is
    if ((quote.creatorId == null && req.user.role === "creator") || quote.creatorId === req.user.id) {
        await prisma.quote.delete({
            where: {
                id: quoteId
            }
        });

        res
            .status(StatusCodes.NO_CONTENT)
            .send();
    } else {
        return res
            .status(StatusCodes.UNAUTHORIZED)
            .send({error: `Can't delete quotes not created by you!`})
    }
});

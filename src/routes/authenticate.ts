import {Router, Request, Response} from "express";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

import {StatusCodes} from "http-status-codes";
import {prisma} from "../prisma/prisma";
import {User} from "@prisma/client";

export const router = Router();


router.post(["/auth/login", "/authenticate"], async (req, res) => {
    // Verkrijg gebruikersnaam een wachtwoord uit het body
    const {username, password} = req.body;

    // Als gebruikersnaam of wachtwoord null is, stuur een status 400
    if (!(username && password)) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: "Username and/or password is missing"});
    }

    const user = await prisma.user.findUnique({
        where: {
            username
        }
    });

    // Zoek naar de gebruiker met het gegeven username
    // Check of er meer dan 0 rows teruggegeven worden (oftewel: is gebruiker gevonden?)
    if (!user) {
        return res
            .status(StatusCodes.UNAUTHORIZED)
            .send({error: "Username not found"});
    }

    // Check of de wachtwoorden overheen komen
    const passwordsAreEqual = await bcrypt.compare(password, user.password);

    // Als de wachtwoorden niet overheen komen stuur een 401 status terug
    if (!passwordsAreEqual) {
        return res
            .status(StatusCodes.UNAUTHORIZED)
            .send({error: "Invalid username and/or password"});
    }

    const token = jwt.sign({id: user.id}, user.secret);
    res
        .status(StatusCodes.CREATED)
        .send({token: token});
});

router.post(["/auth/register", "/users"], async (req, res) => {
    const {username, name, password} = req.body;

    if (!(username && name && password)) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: "Username, password and/or name missing"});
    }

    if (password.length < 8 || username.length < 8 || name.length < 8) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: "Password, username and/or name to short"})
    }

    if (process.env.WHITELIST === "true") {
        const whiteList = await prisma.whitelist.findMany();
        const usernames = whiteList.map((u: User) => u.username);


        if (!usernames.includes(username)) {
            return res
                .status(StatusCodes.CONFLICT)
                .send({error: `Username ${username} not in whitelist`})
        }

        await processUserCreation(req, res, username, name, password);
    } else {
        await processUserCreation(req, res, username, name, password);
    }

});

async function processUserCreation(req: Request, res: Response, username: string, name: string, password: string) {
    const users = await prisma.user.findMany({
        select: {
            username: true
        }
    });

    const usernames = users.map((user: User) => user.username);

    if (usernames.includes(username)) {
        return res
            .status(StatusCodes.CONFLICT)
            .send({error: `Account with username ${username} already created`})
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = await prisma.user.create({
        data: {
            username,
            name,
            password: hashedPassword,
        }
    })

    delete newUser.secret;
    delete newUser.password;

    res
        .status(StatusCodes.CREATED)
        .send(newUser)

}

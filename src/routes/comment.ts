import {Router} from "express";
import {StatusCodes} from "http-status-codes";

import {checkQuoteId, checkQuoteExists} from "../middlewares/quote";
import {checkLoggedIn} from "../middlewares/authorization";

import {isValidId} from "../helpers/methods";
import {prisma} from "../prisma/prisma";

export const router = Router();
router.use(checkLoggedIn)

router.get("/quotes/:quoteId/comments", checkQuoteId, checkQuoteExists, async (req, res) => {
    const quoteId = Number(req.params);

    const commentsOfQuotes = await prisma.comment.findMany({
        where: {
            quoteId,
        }
    })

    res
        .status(StatusCodes.OK)
        .send(commentsOfQuotes);
});

router.post("/quotes/:quoteId/comments", checkQuoteId, checkQuoteExists, async (req, res) => {
    const {text} = req.body;
    const quoteId = Number(req.params.quoteId);

    if (!text) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: `Comment text missing`})
    }

    const newComment = await prisma.comment.create({
        data: {
            quoteId,
            userId: req.user.id,
            text,
        }
    })

    res
        .status(StatusCodes.CREATED)
        .send(newComment);
});


router.delete("/comments/:commentId", async (req, res) => {
    const commentId = Number(req.params.commentId);

    if (!isValidId(commentId)) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: `${commentId} is not a valid comment id`})
    }

    const commentCount = await prisma.comment.count({
        where: {
            id: commentId
        }
    })


    if (commentCount === 0) {
        return res
            .status(StatusCodes.NOT_FOUND)
            .send({error: `Comment with id ${commentId} not found`});
    }

    await prisma.comment.delete({
        where: {
            id: commentId,
        }
    })

    res
        .status(StatusCodes.NO_CONTENT)
        .send();
});
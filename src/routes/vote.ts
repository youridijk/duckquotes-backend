import {Router, Request} from "express";
import {StatusCodes} from "http-status-codes";
import {checkLoggedIn} from "../middlewares/authorization";
import {checkQuoteId, checkQuoteExists} from "../middlewares/quote";

export const router = Router({mergeParams: true});
router.use(checkQuoteId);
router.use(checkLoggedIn);

import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient()

router.get("", checkQuoteExists, async (req, res) => {
    const quoteId = Number(req.params.quoteId);

    const votesOfQuote = await prisma.vote.findMany({
        where: {
            quoteId
        },
        include: {
            user: {
                select: {
                    username: true
                }
            }
        }
    })

    res
        .status(StatusCodes.OK)
        .send(votesOfQuote);
});

router.post("", async (req: Request, res) => {
    const quoteId = Number(req.params.quoteId);
    const userId = req.user.id;

    const {isPositive} = req.body;

    if(isPositive != true && isPositive != false){
        console.log(req.body)
        return res
            .status(StatusCodes.BAD_REQUEST)
            .send({error: "isPositive key missing or invalid"})
    }

    const quoteCount = await prisma.quote.count({
        where: {
            id: quoteId,
        }
    })

    if (quoteCount === 0) {
        return res
            .status(StatusCodes.NOT_FOUND)
            .send({error: `Quote with id ${quoteId} not found`})
    }

    await prisma.vote.upsert({
        where: {
            quoteId_userId: {
                quoteId,
                userId
            }
        },
        update: {
            isPositive,
        },
        create: {
            quoteId: quoteId,
            isPositive,
            userId
        }
    })

    res
        .status(201)
        .send();
});


router.delete("", async (req: Request, res) => {
    const quoteId = Number(req.params.quoteId);
    const {id: userId} = req.user;

    const quoteCount = await prisma.vote.count({
        where: {
            userId,
            quoteId,
        }
    });

    if (quoteCount === 0) {
        console.log("NOT FOUND")
        return res
            .status(StatusCodes.NOT_FOUND)
            .send({error: `Vote with quoteId ${quoteId} and userId ${userId} not found`});
    }


    await prisma.vote.delete({
        where: {
            quoteId_userId: {
                quoteId,
                userId
            }
        },
    })

    res
        .status(StatusCodes.NO_CONTENT)
        .send()
});
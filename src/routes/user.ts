import {StatusCodes} from "http-status-codes";
import {User} from "@prisma/client";
import {Response, Router} from "express";

import {prisma} from "../prisma/prisma";

const {checkLoggedIn, checkCreator} = require("../middlewares/authorization");

export const router = Router();

router.get("", checkLoggedIn, (req, res: Response) => {
    const userdata = req.user;
    delete userdata.secret;
    delete userdata.password;

    res
        .status(StatusCodes.OK)
        .send(userdata)
});

router.get("/usernames", checkLoggedIn, async (req, res) => {
    const users = await prisma.user.findMany({
        where: {
            AND: [
                {
                    id: {
                        gt: 0
                    }
                },
                {
                    NOT: {
                        username: {
                            equals: "apple_tester"
                        }
                    }
                }
            ]

        },
        select: {
            id: true,
            username: true
        },
        orderBy: {
            id: "asc"
        }
    })

    res
        .status(StatusCodes.OK)
        .send(users);
});

router.get("/whitelist/status", checkLoggedIn, checkCreator, (req, res) => {
    res
        .status(StatusCodes.OK)
        .send({status: process.env.WHITELIST});
});

router.get("/whitelist", checkLoggedIn, async (req, res) => {
    const whiteList = await prisma.whitelist.findMany();
    const usernames = whiteList.map((user: User) => user.username)

    res
        .status(StatusCodes.OK)
        .send(usernames);
});

router.post("/whitelist", checkLoggedIn, checkCreator, (req, res) => {
    const whiteListStatus = process.env.WHITELIST;
    process.env.WHITELIST = whiteListStatus === "true" ? "false" : "true"
    res
        .status(StatusCodes.OK)
        .send({status: process.env.WHITELIST});
});

router.post("/whitelist/:username", checkLoggedIn, checkCreator, async (req, res) => {
    const {username} = req.params;

    const whiteListCount = await prisma.whitelist.count({
        where: {
            username
        }
    });

    if (whiteListCount === 0) {
        await prisma.whitelist.create({
            data: {
                username
            }
        });
        res
            .status(StatusCodes.CREATED)
            .send();
    } else {
        res
            .status(StatusCodes.CONFLICT)
            .send({error: `Username ${username} already on whitelist`})
    }
});

router.delete("/:userId", checkLoggedIn, async (req, res) => {

    await prisma.user.delete({
        where: {
            id: req.user.id,
        }
    })

    res
        .status(StatusCodes.NO_CONTENT)
        .send();
});

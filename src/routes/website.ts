import express, {Router} from "express";
import {StatusCodes} from "http-status-codes";
import fs from "fs";
import path from "path";

export const router = Router();
import {prisma} from "../prisma/prisma";

router.get("/downloads", async (req, res) => {
    const latestVersion = await prisma.versionHistory.findFirst({
        orderBy: {
            id: "desc"
        }
    });

    const filePath = path.resolve(__dirname, "../../src/web/downloads.html")
    let fileText = fs.readFileSync(filePath).toString("utf-8");


    if (!latestVersion) {
        fileText = fileText.replace("${VERSION}", "Error ophalen versie uit database");
    } else {
        fileText = fileText
            .replace("${VERSION}", latestVersion.version)
            .replace("${ANDROID}", latestVersion.androidDownload)
            .replace("${IOS}", latestVersion.iosDownload)
    }

    res
        .status(StatusCodes.OK)
        .setHeader("Content-Type", "text/html")
        .send(fileText);
});

router.get("/downloads/ios", async (req, res) => {
    const latestVersion = await prisma.versionHistory.findFirst({
        orderBy: {
            id: "desc"
        },
        select: {
            iosDownload: true,
        }
    });

    if (!latestVersion) {
        res
            .status(StatusCodes.INTERNAL_SERVER_ERROR)
            .send(`<h1>Error getting iOS download: Latest version not found</h1>`)
    } else {
        res
            .status(StatusCodes.OK)
            .redirect(latestVersion.iosDownload)
    }
});

router.get("/downloads/android", async (req, res) => {
    const latestVersion = await prisma.versionHistory.findFirst({
        orderBy: {
            id: "desc"
        },
        select: {
            androidDownload: true,
        }
    });

    if (!latestVersion) {
        res
            .status(StatusCodes.INTERNAL_SERVER_ERROR)
            .send(`<h1>Error getting iOS download: Latest version not found</h1>`)
    } else {
        res
            .status(StatusCodes.OK)
            .redirect(latestVersion.androidDownload)
    }
});

router.get("/downloads/auto", (req, res) => {
    const useragent = req.useragent;
    let redirectUrl = "/downloads";

    if (useragent.isiPhone) {
        redirectUrl += "/ios";
    } else if (useragent.isAndroid) {
        redirectUrl += "/android";
    }

    res
        .status(StatusCodes.OK)
        .redirect(redirectUrl)
});

router.use(express.static(path.resolve(__dirname, "../../src/web")))

import {User, Quote} from "@prisma/client";

declare global {
    namespace Express {
        interface Request {
            user?: User,
            quote? : Quote ,
        }
    }
}
import useragent from "express-useragent";
import {loadEnv} from "./helpers/env-loader";
import express from "express";

loadEnv();
const app = express();

// Express gebruik bij lokaal testen altijd 8080
// alleen Heroku (hosting provider) gebruikt een poort die gedefinieerd is in de $PORT env var
const PORT = process.env.PORT || 8080;
const enableCORS = process.env.cors === "true" || true;
const isServer = process.env.IS_SERVER  === "true" || false;

if(process.env.WHITELIST == null){
    process.env.WHITELIST = "true";
}

if(enableCORS){
    console.log("CORS turned on");
    app.use(require("cors")());
}

if(!isServer) {
    // simpele Logger middleware
    app.use((req, res, next) => {
        console.log(`${req.method} on ${req.originalUrl}`)
        next();
    });
}

app.use(express.json());
app.use(useragent.express());

app.use("/api/v1/users", require("./routes/user").router);
app.use("/api/v1/quotes", require("./routes/quote").router);
app.use("/api/v1", require("./routes/authenticate").router);
app.use("/api/v1/", require("./routes/comment").router);

app.use(require("./routes/website").router);

// Start express server
app.listen(PORT, () => {
    console.log(`Express is running on http://localhost:${PORT}`);
})
